package telran;

import telran.item.ExitThreadsItem;
import telran.item.StartRaceItem;
import telran.menu.ConsoleInputOutput;
import telran.menu.InputOutput;
import telran.menu.Item;
import telran.menu.Menu;

public class ThreadsRaceAppl {
    private static final InputOutput inputOutput = new ConsoleInputOutput();

    public static void main(String[] args) {
        Item[] items = {
                new StartRaceItem(inputOutput),
                new ExitThreadsItem(inputOutput),
        };
        Menu menu = new Menu(items, inputOutput);
        menu.menuRun();
    }
}
