package telran;

import telran.item.StartRaceItem;

import java.util.stream.IntStream;

public class ThreadsRaceImpl extends Thread {
    private static final int MIN = 2;
    private static final int MAX = 5;

    private final int distanceIterations;
    public String name;

    public ThreadsRaceImpl(String name, int distanceIterations) {
        this.name = name;
        this.distanceIterations = distanceIterations;
    }

    private static int getRandom() {
        return (int) (MIN + Math.random() * (MAX - MIN + 1));
    }

    @Override
    public void run() {
        IntStream.range(0, distanceIterations).forEachOrdered(i -> {
            try {
                int sleepTime = getRandom();
                System.out.println("Distance iterations # = " + (i + 1)
                        + " #" + name + " Sleep Time = " + sleepTime);
                sleep(sleepTime);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });
        StartRaceItem.setWinner();
    }
}
