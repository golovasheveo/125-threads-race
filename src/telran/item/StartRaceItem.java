package telran.item;

import telran.ThreadsRaceImpl;
import telran.menu.InputOutput;
import telran.menu.Item;

public class StartRaceItem implements Item {
    private static ThreadsRaceImpl gameWinner = null;
    private final InputOutput inputOutput;

    public StartRaceItem(InputOutput inputOutput) {
        super();
        this.inputOutput = inputOutput;
    }

    public synchronized static void setWinner() {
        if (gameWinner == null) gameWinner = (ThreadsRaceImpl) Thread.currentThread();
    }

    @Override
    public String displayName() {
        return "Start Threads-Race Game";
    }

    @Override
    public void perform() {

        gameWinner = null;

        int threads = inputOutput.inputInteger("Enter number of threads:", 2, 1000);
        int distance = inputOutput.inputInteger("Enter distance:", 2, 1000);

        try {
            startThreads(threads, distance);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("Congratulations to thread #" + gameWinner.name);
    }

    private void startThreads(int threads, int distance) throws InterruptedException {
        ThreadsRaceImpl[] threadsRaceImpl = new ThreadsRaceImpl[threads];

        for (int i = 0; i < threads; i++) {
            threadsRaceImpl[i] = new ThreadsRaceImpl("Thread" + i, distance);
            threadsRaceImpl[i].start();
        }

        joinRacers(threadsRaceImpl);
    }

    private void joinRacers(ThreadsRaceImpl[] threadsRaceImpl) throws InterruptedException {
        for (ThreadsRaceImpl threadsRace : threadsRaceImpl) {
            threadsRace.join();
        }
    }
}
